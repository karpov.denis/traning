const OFF = 'off';
const ON = 'on';
const END = 'end';
const PAUSE = 'pause';
const MAX_SECONDS = 3540;
const MINUTE = 60;

export {
  OFF,
  ON,
  PAUSE,
  END,
  MAX_SECONDS,
  MINUTE,
}