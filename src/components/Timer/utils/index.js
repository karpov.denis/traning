export function toTwoDigitFormat(number) {
    return number.toString().padStart(2,0);
};

export function getMinutesFromTime(sec) {
    return (sec - sec%60)/60;
};
  
export function getSecondsFromTime(sec) {
    return sec%60;
};
