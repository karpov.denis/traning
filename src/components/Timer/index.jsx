import cn from 'classnames';
import React, { Component } from 'react';
import { func, bool, number, object } from 'prop-types';

import './style.css';
import {
  toTwoDigitFormat,
  getMinutesFromTime,
  getSecondsFromTime,
} from './utils';
import {
  OFF,
  ON,
  END,
  PAUSE,
  MAX_SECONDS,
  MINUTE,
} from './constants';


class Timer extends Component {
  state = {
    seconds: null,
    timerState: OFF,
  }

  constructor(props) {
    super(props);

    const { seconds = 0 , minutes = 0, } = props.time;

    this.audio = new Audio('/audio/pick.mp3');
    this.state.seconds = minutes * MINUTE + seconds;
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  get timerEndingHighlight() {
    const { timerEnding } = this.props;
    const { seconds, timerState, } = this.state;

    return seconds < timerEnding && timerState !== OFF;
  }

  get startBtnOn() {
    const { timerState } = this.state;

    return timerState === OFF || timerState === PAUSE;
  }

  get activeControlsBtn() {
    const { controls } = this.props;
    const { timerState } = this.state;

    return controls && timerState === OFF;
  }

  tick = () => {
    const { stop } = this.props;
    let { seconds } = this.state;

    if (stop) {
      this.reset();

      return;
    };
    
    seconds -= 1;
    this.setState({ seconds });

    if (seconds === 0) {
      this.audio.play();
      clearInterval(this.intervalID);
      this.props.callback();
      this.setState({ timerState: END });
    };

    if (seconds < 0) {
      clearInterval(this.intervalID);
      this.setState({ seconds: 0 });
    }
  }

  start = () => {
    const { seconds, timerState, } = this.state;
    
    if (seconds === 0 || timerState === ON) {
      return;
    };

    this.intervalID = setInterval(this.tick, 1000);
    this.setState({ timerState: ON });
  }

  increment = () => {
    let { seconds } = this.state;
    seconds = seconds < MAX_SECONDS ? seconds + MINUTE : 0;
    this.setState({ seconds });
  }

  decrement = () => {
    let { seconds } = this.state;
    seconds = seconds > 0 ? seconds - MINUTE : MAX_SECONDS;
    this.setState({ seconds });
  }

  pause = () => {
    clearInterval(this.intervalID);
    this.setState({ timerState: PAUSE });
  }

  reset = () => {
    clearInterval(this.intervalID);

    this.audio.pause();
    this.currentTime = 0;

    this.setState({
      timerState: OFF,
      seconds: 0,
      timerEndingHighlight: false,
    });
  }

  render() {
    const { controls } = this.props;
    const { seconds, timerState, } = this.state;

    return (
      <div className={cn("timer-box", { "timer-box--highlighting": timerState === END })}>
        <h2 className="timer-box__title">Timer</h2>
        <div className="time">
          <div className="time-wrapper">
            {this.activeControlsBtn && (
              <>
                <span className="btn-minute--plus" onClick={ this.increment } /> 
                <span className="btn-minute--minus" onClick={ this.decrement } />
              </>
            )}  
            <div className="time__minute">
              { toTwoDigitFormat(getMinutesFromTime(seconds)) }
            </div>
          </div>
          <span className="time__colon">:</span>
          <div className={cn("time__seconds", { 'time__seconds--highlighting': this.timerEndingHighlight })}>
            { toTwoDigitFormat(getSecondsFromTime(seconds)) }
          </div>
        </div>
        <div className="timer-btn-box">
          {controls && (
            <>
              {this.startBtnOn && <button className="timer-btn timer-btn--start" onClick={ this.start }>Start</button>}
              {timerState === ON && <button className="timer-btn timer-btn--pause" onClick={ this.pause }>Pause</button>}
              {timerState === END && <button className="timer-btn timer-btn--off" onClick={ this.reset }>Turn Off</button>}
            </>
          )}
        </div>
      </div>  
    );
  }
};

Timer.defaultProps = {
  callback: () => null,
  timerEnding: 10,
};

Timer.propTypes = {
  callback: func,
  controls: bool,
  timerEnding: number,
  time: object,
};

export default Timer;
