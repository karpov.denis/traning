import React, { Component } from 'react';

import { Timer } from '@components';
import './styles.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.timerRef = React.createRef();
  }

  startTimer = () => this.timerRef.current.start();

  stopTimer = () => this.timerRef.current.reset();

  render(){
    const time = {
      seconds: 15,
      minutes: 0,
    };

    return (
      <>
        <div className="app-btn-box">
          <button className="app-btn app-btn--start" onClick={ this.startTimer }>START</button>
          <button className="app-btn app-btn--stop" onClick={ this.stopTimer }>STOP</button>
        </div>
        <div>
          <Timer ref={ this.timerRef } time={ time } controls />
        </div>
      </>
    );
  }
};

export default App;
